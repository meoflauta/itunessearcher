//
//  EmptyStateView.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/4/21.
//

import Foundation
import SwiftUI

/// View displayed when there is no searched data
struct EmptyStateView: View {
    var body: some View {
        VStack{
            Spacer()
            HStack{
                Image(systemName: "music.note")
                    .font(.system(size: 70))
                    .padding(.bottom)
                Image(systemName: "film")
                    .font(.system(size: 70))
                    .padding(.bottom)
            }
            HStack{
                Image(systemName: "tv")
                    .font(.system(size: 70))
                    .padding(.bottom)
                Image(systemName: "airplayaudio")
                    .font(.system(size: 70))
                    .padding(.bottom)
            }
            
            Text("Start searching for music, movies, ebooks, podcasts")
                .font(.title)
                .foregroundColor(.black)
            Spacer()
        }
        .padding()
        .foregroundColor(Color(.systemPink))
    }
}

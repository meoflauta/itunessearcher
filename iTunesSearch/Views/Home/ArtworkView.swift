//
//  ArtworkView.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/4/21.
//

import SwiftUI
import Kingfisher

/// Displays the image of the media
struct ArtworkView: View {
    let image: Image?
    var body: some View {
        ZStack{
            if image != nil {
                image!
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 60, height: 60)
                    .clipped()
            }else{
                Color(.systemIndigo)
                Image(systemName: "music.note")
                    .font(.largeTitle)
                    .foregroundColor(.white)
            }
        }
        .frame(width: 60.0, height: 60.0)
        .shadow(radius: 5.0)
        .padding(.trailing, 5.0)
    }
}

struct ArtworkView_Previews: PreviewProvider {
    static var previews: some View {
        ArtworkView(image: Image(systemName: "music.note"))
    }
}

/// Alternative image view that could be used later but is not used yet
struct KingfisherImageView: View {
    let url: URL
    let placeholderImage: UIImage
    var body: some View {
        KFImage.url(url)
          .loadDiskFileSynchronously()
          .cacheOriginalImage()
          .fade(duration: 0.25)
          .onProgress { receivedSize, totalSize in}
          .onSuccess { result in  }
          .onFailure { error in }
          .resizable()
          .aspectRatio(contentMode: .fit)
          .frame(width: 60, height: 60)
          .clipped()
          .shadow(radius: 5.0)
          .padding(.trailing, 5.0)
    }
}

//
//  ContentView.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/3/21.
//

import SwiftUI
import Combine
import ComposableArchitecture
import Kingfisher

/// Home view
struct ContentView: View {
    @ObservedObject var viewModel: MediaListViewModel
    @ObservedObject var monitor = NetworkMonitor()
    
    var body: some View {
        NavigationView{
            VStack{
                VStack(alignment: .leading, spacing: 0.0){
                    EntityPicker(entity: $viewModel.entity, entities: viewModel.entities)
                    SearchBar(searchTerm: $viewModel.searchTerm, entity: Entity(rawValue: viewModel.entity)!)
                    Text( monitor.isConnected ? "" : "You are offline")
                        .foregroundColor(.red)
                        .font(.footnote)
                        .padding(.leading, 8.0)
                }
                
                if viewModel.media.isEmpty {
                    EmptyStateView()
                }else{
                    
                    List(viewModel.media){ mediaModel in
                        NavigationLink(destination:  DetailView(entity: Entity(rawValue: viewModel.entity)! , mediaVM: mediaModel)) {
                            MediaView(media: mediaModel, entity: Entity(rawValue: viewModel.entity)! )
                        }
                    }
                    .listStyle(PlainListStyle())
                }
            }
            .navigationBarTitle("iTunes Search")
        }
        .accentColor( .black) 
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewModel: MediaListViewModel())
    }
}

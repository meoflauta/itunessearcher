//
//  MediaView.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/4/21.
//

import SwiftUI

/// View for each row of media; KingfisherImageView can be used as an alternative to ArtworkView
struct MediaView: View {
    @ObservedObject var media: MediaViewModel
    var entity: Entity
    var body: some View {
        HStack{
//            KingfisherImageView(url: URL(string: media.artworkUrlString)!, placeholderImage: UIImage(systemName: entity.placeholderImage)!)
//                .padding(.trailing)
            ArtworkView(image: media.artwork)
                .padding(.trailing)
            VStack(alignment: .leading) {
                Text(media.trackName)
                Text(media.artistName)
                    .font(.footnote)
                    .foregroundColor(.gray)
                HStack{
                    Text(media.genre)
                        .font(.footnote)
                    Spacer()
                    Text("$\(media.price)")
                        .font(.footnote)
                        .foregroundColor(.green)
                }
            }
        }
        .padding(.trailing, 5.0)
    }
}

struct MediaView_Previews: PreviewProvider {
    static var previews: some View {
        MediaView(media: MediaViewModel(medium: Media(id: 1, trackName: "Tarzan", artistName: "Steven Kostner", artworkUrl: "https://www.apple.com", price: 1.65, genre: "science fiction", kind: "movie", description: "This is a long movie about how Tarzan went to Mars", longDescription: "This is a very long description about the movie")), entity: Entity.movie)
    }
}

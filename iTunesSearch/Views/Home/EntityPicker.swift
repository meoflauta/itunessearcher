//
//  EntityPicker.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/4/21.
//

import Foundation
import SwiftUI

/// Allows user to select media type
struct EntityPicker: View {
    
    @Binding var entity: String
    var entities: [String]

    var body: some View {
        VStack {
            Picker("What are you looking for?", selection: $entity) {
                ForEach(entities, id: \.self) {
                    Text($0)
                }
            }
            .pickerStyle(.segmented)
            .padding(.leading, 10.0)
            .padding(.trailing, 10.0)
        }
    }
}

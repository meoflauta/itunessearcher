//
//  DetailView.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/3/21.
//

import SwiftUI

/// Displays the detail view of the specific media
struct DetailView: View {
    var entity: Entity = Entity.song
    var mediaVM: MediaViewModel!
    var body: some View {
        ScrollView{
            VStack{
                ZStack{
                    if mediaVM.artwork != nil {
                        mediaVM.artwork!
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 380, height: 100)
                            .clipped()
                    }else{
                        Color(.systemIndigo)
                        Image(systemName: "music.note")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 380, height: 100)
                            .clipped()
                    }
                }
                    
                VStack(spacing: 16.0){
                    VStack{
                        Text(mediaVM.trackName)
                            .font(.title)
                            .multilineTextAlignment(.center)
                        Text(mediaVM.genre)
                        Text(mediaVM.artistName)
                    }
                    Text(mediaVM.description)
                }
                .padding(.top)
                .padding(.horizontal, 12.0)
            }
            .padding(.leading)
            .padding(.trailing)
            .padding(.bottom)
        }
        .navigationBarTitleDisplayMode(.inline)
        .padding(.top, 20.0)
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView()
    }
}

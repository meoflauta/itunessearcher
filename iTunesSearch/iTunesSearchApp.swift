//
//  iTunesSearchApp.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/3/21.
//

import SwiftUI

/// Represents the whole app
@main
struct iTunesSearchApp: App {

    ///Create the first view and inject its view model
    var body: some Scene {
        WindowGroup {
            ContentView(viewModel: MediaListViewModel())
        }
    }
}

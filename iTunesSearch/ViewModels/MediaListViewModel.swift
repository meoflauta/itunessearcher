//
//  MediaListViewModel.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/3/21.
//

import Combine
import Foundation
import SwiftUI
import CoreData
import Network


/// Defines the business logic for the Content View
class MediaListViewModel: ObservableObject {
    
    @Published var searchTerm: String = ""
    @Published public private(set) var media: [MediaViewModel] = []
    
    @Published var entity: String = Entity.movie.rawValue
    
    @ObservedObject var monitor = NetworkMonitor()
    
    var entities = [Entity.movie.rawValue, Entity.song.rawValue, Entity.tvEpisode.rawValue, Entity.podcast.rawValue]
    
    private let dataModel: DataModel = DataModel()
    private let artworkLoader: ArtworkLoader = ArtworkLoader()
    private var disposables = Set<AnyCancellable>()
        
    init(){
        $searchTerm
            .sink(receiveValue: loadMedia(searchTerm:))
            .store(in: &disposables)
        
    }
    
    ///Load media either from online inside the if condition, or from coredata inside the else
    private func loadMedia(searchTerm: String) {
        media.removeAll()
        artworkLoader.reset()
        guard let entityObject = Entity(rawValue: self.entity) else {return}
        
        if monitor.isConnected {
            dataModel.loadMedia(searchTerm: searchTerm, entity: entityObject.uploadEntity) { mediaList in
                mediaList.forEach{ self.appendMedium(medium: $0)}
                if mediaList.count > 0 {
                    CDStorage.shared.saveInCoreData(mediaList: mediaList, entity: entityObject)
                }
            }
        }
        else{
            guard let entity: Entity = Entity(rawValue: self.entity) else {return}
            CDStorage.shared.fetch(kind: entity.uploadEntity, searchTerm: searchTerm) { [weak self] cdMediaList in
                for cdMedia in cdMediaList {
                    guard let media: Media = self?.transformCDMediaToMedia(cdMedia: cdMedia) else {continue}
                    self?.appendMedium(medium: media)
                }
            }
        }
    }
    
    ///convert CoreData object CDMedia to universal Media
    func transformCDMediaToMedia(cdMedia: CDMedia) -> Media {
        var description = ""
        if cdMedia.shortDesc != nil && cdMedia.shortDesc!.count > 0 {
            description = cdMedia.shortDesc!
        }else if cdMedia.longDesc != nil && cdMedia.longDesc!.count > 0 {
            description = cdMedia.longDesc!
        }
        let media = Media(id: Int(cdMedia.id), trackName: cdMedia.trackName ?? "", artistName: cdMedia.artistName ?? "", artworkUrl: cdMedia.artworkUrl ?? "", price: cdMedia.price, genre: cdMedia.genre ?? "", kind: cdMedia.kind ?? "movie", description: description, longDescription: cdMedia.longDesc)
        return media
    }
    
    /// append the downloaded media for a row to the view model for that specific row then kicks off download of artwork
    private func appendMedium(medium:Media){
        let mediaViewModel = MediaViewModel(medium: medium)
        DispatchQueue.main.async{
            self.media.append(mediaViewModel)
        }
        artworkLoader.loadArtwork(forMedium: medium) { image in
            DispatchQueue.main.async{
                mediaViewModel.artwork = image
            }
        }
    }
}

/// View Model for each media that also corresponds to each row in the table
class MediaViewModel: Identifiable, ObservableObject {
    let id: Int
    let trackName: String
    let artistName: String
    let price: String
    let genre: String
    let description: String
    let artworkUrlString: String
    @Published var artwork: Image?
    
    init(medium: Media){
        self.id = medium.id
        self.trackName = medium.trackName
        self.artistName = medium.artistName
        self.price = String(format: "%.2f", medium.price)
        self.genre = medium.genre
        self.artworkUrlString = medium.artworkUrl
        
        if let description = medium.description{
            self.description = description
        }else if let longDescription = medium.longDescription{
            self.description = longDescription
        }else {
            self.description = ""
        }
    }
}


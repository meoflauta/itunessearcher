//
//  Entity.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/4/21.
//

import Foundation

/// Enumerates the types of media in the app
enum Entity: String {
    case movie
    case song
    case tvEpisode = "tv episode"
    case podcast
    
    /// used when filtering the search entity in iTunes and in CoreData
    var uploadEntity: String {
        switch self {
        case .movie:
            return "movie"
        case .song:
            return "song"
        case .tvEpisode:
            return "tvEpisode"
        case .podcast:
            return "podcast"
        }
    }
    
    /// To dynamically change placeholder based on media type
    var placeholderText: String {
        switch self {
        case .movie:
            return "Enter a movie title, director, actor..."
        case .song:
            return "Enter a song, artist, album... "
        case .tvEpisode:
            return "Enter an episode title, actors, actresses..."
        case .podcast:
            return "Enter a title, author..."
        }
    }
    
    /// To dynamically change placeholder image based on media type
    var placeholderImage: String {
        switch self {
        case .movie:
            return "film"
        case .song:
            return "music.note"
        case .tvEpisode:
            return "tv"
        case .podcast:
            return "airplayaudio"
        }
    }
}

//
//  NetworkMonitor.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/3/21.
//

import Foundation
import Network
import Combine

/// Detects if the iOS device is online or offline
final class NetworkMonitor: ObservableObject {
    let monitor = NWPathMonitor()
    let queue = DispatchQueue(label: "Monitor")
    
    @Published var isConnected: Bool = true
    
    init(){
        monitor.pathUpdateHandler = { [weak self] path in
            switch path.status {
            case .satisfied:
                OperationQueue.main.addOperation {
                    self?.isConnected = true
                    debugPrint("connected")
                }
            case .unsatisfied:
                OperationQueue.main.addOperation {
                    self?.isConnected = false
                    debugPrint("not connected")
                }
            case .requiresConnection:
                break
            @unknown default:
                OperationQueue.main.addOperation {
                    self?.isConnected = false
                    debugPrint("not connected default")
                }
            }
        }
        
        monitor.start(queue: queue)
    }
}

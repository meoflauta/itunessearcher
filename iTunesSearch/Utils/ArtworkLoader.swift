//
//  ArtworkLoader.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/3/21.
//

import Foundation
import SwiftUI

/// Loads artwork images of the tableview
class ArtworkLoader {
    private var dataTasks: [URLSessionDataTask] = []
    
    ///creates a single download task for a row and appends that to the collection of download tasks for the whole table
    func loadArtwork(forMedium medium: Media, completion: @escaping((Image?) -> Void)){
        guard let imageUrl = URL(string: medium.artworkUrl) else {
            completion(nil)
            return
        }
        
        let dataTask = URLSession.shared.dataTask(with: imageUrl) { data, _, _ in
            guard let data = data, let artwork = UIImage(data: data) else {
                completion(nil)
                return
            }
            let image = Image(uiImage: artwork)
            completion(image)
        }
        dataTasks.append(dataTask)
        dataTask.resume()
    }
    
    func reset() {
        dataTasks.forEach{ $0.cancel() }
    }
}

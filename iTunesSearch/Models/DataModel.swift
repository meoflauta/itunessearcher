//
//  DataModel.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/3/21.
//

import Foundation

///  Data model for a media type and contains downloading functionality for the media
class DataModel {
    private var dataTask: URLSessionDataTask?
    
    ///builds and kicks off the download data task for a single search based on the selected searched term and entity
    func loadMedia(searchTerm: String, entity: String, completion: @escaping(([Media]) -> Void)) {
        dataTask?.cancel()
        guard let url = buildUrl(forTerm: searchTerm, entity: entity) else {
            completion([])
            return
        }
        
        dataTask = URLSession.shared.dataTask(with: url) { data, _, _ in
            guard let data = data else {
                completion([])
                return
            }
            if let mediaResponse = try? JSONDecoder().decode(MediaResponse.self, from: data){
                completion(mediaResponse.media)
            }
        }
        
        dataTask?.resume()
    }
    
    ///Constructs the itunes search url based on the search term and entity
    private func buildUrl(forTerm searchTerm: String, entity: String) -> URL? {
        guard !searchTerm.isEmpty else {return nil}
        
        let queryItems = [
            URLQueryItem(name: "term", value: searchTerm),
            URLQueryItem(name: "entity", value: entity),
            
        ]
        var components = URLComponents(string: "https://itunes.apple.com/search")
        components?.queryItems = queryItems
        
        return components?.url
    }
}

/// Represents the whole response of the search for automatic decoding
struct MediaResponse: Decodable {
    let media: [Media]
    
    enum CodingKeys: String, CodingKey {
        case media = "results"
    }
}

/// Represents each media in the search for automatic decoding
struct Media: Decodable {
    let id: Int
    let trackName: String
    let artistName: String
    let artworkUrl: String
    let price: Float
    let genre: String
    var kind: String
    let description: String?
    let longDescription: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "trackId"
        case trackName
        case artistName
        case artworkUrl = "artworkUrl100"
        case price = "trackPrice"
        case genre = "primaryGenreName"
        case kind
        case description
        case longDescription
    }
}

//
//  PersistenceController.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/3/21.
//

import Foundation
import SwiftUI
import CoreData

/// A singleton that enables CoreData access by all View Models;
struct PersistenceController {
    
    static let shared = PersistenceController()

    /// Storage for Core Data
    let container: NSPersistentContainer

    /// An initializer to load Core Data, optionally able to use an in-memory store.
    init(inMemory: Bool = false) {
        /// Change if model CDMain is not named as below.
        container = NSPersistentContainer(name: "CDMain")

        if inMemory {
            container.persistentStoreDescriptions.first?.url = URL(fileURLWithPath: "/dev/null")
        }

        container.loadPersistentStores { description, error in
            if let error = error {
                fatalError("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func save() {
        let context = container.viewContext

        if context.hasChanges {
            do {
                try context.save()
            } catch {
                print("Unable to save in CoreData")
            }
        }
    }
}

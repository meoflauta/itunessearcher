//
//  CDMedia+CoreDataClass.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/3/21.
//
//

import Foundation
import CoreData

///Data model class which is the equivalent of the Media model when saved in CoreData
@objc(CDMedia)
public class CDMedia: NSManagedObject {

}

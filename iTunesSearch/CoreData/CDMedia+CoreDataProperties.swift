//
//  CDMedia+CoreDataProperties.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/3/21.
//
//

import Foundation
import CoreData


extension CDMedia {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDMedia> {
        return NSFetchRequest<CDMedia>(entityName: "CDMedia")
    }

    @NSManaged public var id: Int64
    @NSManaged public var trackName: String?
    @NSManaged public var artistName: String?
    @NSManaged public var artworkUrl: String?
    @NSManaged public var price: Float
    @NSManaged public var genre: String?
    @NSManaged public var kind: String?
    @NSManaged public var shortDesc: String?
    @NSManaged public var longDesc: String?

}

extension CDMedia : Identifiable {

}

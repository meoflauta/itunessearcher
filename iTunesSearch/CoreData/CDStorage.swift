//
//  CDStorage.swift
//  iTunesSearch
//
//  Created by Romeo Flauta on 10/3/21.
//  https://betterprogramming.pub/swiftui-and-coredata-the-mvvm-way-ab9847cbff0f

import Foundation
import SwiftUI
import CoreData
import Combine

/// Contains the fetchresultscontroller that does the actual saving and fetching to and from CoreData
class CDStorage: NSObject, ObservableObject {
    
    var mediaList = CurrentValueSubject<[CDMedia], Never>([])
    private let mediaFetchController: NSFetchedResultsController<CDMedia>
    
    static let shared: CDStorage = CDStorage()
    
    private override init(){
        
        let fetchRequest: NSFetchRequest<CDMedia> = CDMedia.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "trackName", ascending: true)]
        mediaFetchController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: PersistenceController.shared.container.viewContext,
            sectionNameKeyPath: nil,
            cacheName: nil
        )

        super.init()
    }
    
    /// Fetch from CoreData based on search term entered in search bar and the kind of media selected in segmented control
    func fetch(kind: String, searchTerm: String, completion: @escaping( ([CDMedia]) -> Void)){
        
        let predicate:NSPredicate = NSPredicate(format: "(kind contains [c] %@) AND ((trackName contains [c] %@) OR (artistName contains[c] %@) OR (genre contains [c] %@))", kind, searchTerm, searchTerm, searchTerm)
        mediaFetchController.fetchRequest.predicate = predicate
        
        do {
            try mediaFetchController.performFetch()
            mediaList.value = mediaFetchController.fetchedObjects ?? []
            completion(mediaList.value)
        }catch {
            print("could not fetch objects")
            completion([])
        }
    }
    
    /// Save objects in CoreData
    func saveInCoreData(mediaList: [Media], entity: Entity){
        for media in mediaList {
            let cdMedia = CDMedia(context: mediaFetchController.managedObjectContext)
            cdMedia.id = Int64(media.id)
            cdMedia.trackName = media.trackName
            cdMedia.artistName = media.artistName
            cdMedia.artworkUrl = media.artworkUrl
            cdMedia.price = media.price
            cdMedia.kind = entity.uploadEntity
            cdMedia.shortDesc = media.description
            cdMedia.longDesc = media.longDescription
            cdMedia.genre = media.genre
            
            do {
                try mediaFetchController.managedObjectContext.save()
            } catch(let error) {
                print("core data saving error; create error handler: \(error.localizedDescription)")
            }
            
        }
    }
}

extension CDStorage: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        guard let mediaList = controller.fetchedObjects as? [CDMedia] else {return}
        
        self.mediaList.value = mediaList
    }
}
